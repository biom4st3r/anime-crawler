import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as Bs
import time
from collections import OrderedDict
import cfscrape

class UrlBuilder():
    prefix : str = "https://"
    url : str = ""
    path : str = ""
    
    def useTls(self, boolean : bool):
        if(boolean):
            prefix = "https://"
        else:
            prefix = "http://"
        return self

    def setUrl(self,url : str) :
        self.url = url
        return self

    def animeName(self,name : str) :
        path = name
        return self
    
    def kissAnimeHomePageBuilder(self):
        name = self.path
        path = "/Anime/" + name.replace(' ','-')
        return self

    def makeAnimeHomepage(self):
        if("kiss-anime" in self.url):
            return self.kissAnimeHomePageBuilder()
        elif("darkanime" in self.url):
            return self
        else:
            raise NotImplementedError("that website is no currently supported")

    def build(self) -> str:
        return self.prefix+self.url+self.path


def getCookies() -> dict:
    global cookies
    driver = webdriver.Firefox(executable_path="A:/geckodriver.exe")
    driver.get("https://www.kiss-anime.ws")
    driver.implicitly_wait(10)
    time.sleep(10)
    newcookies = driver.get_cookies()
    cookies = {str(newcookies[0].get("name")):str(newcookies[0].get("value")),str(newcookies[1].get("name")):str(newcookies[1].get("value"))}
    driver.close()
    driver.quit()
    #__cfduid = input("Please enter the __cfduid cookie from your browser:\n> ")
    #cf_clearance = input("Please enter the cf_clearance cookie from your browser:\n> ")
    #cookies = {"__cfduid":str(__cfduid), "cf_clearance":str(cf_clearance)}

def badCookies(r : requests) -> bool: 
    output = Bs(r.text,features="html.parser")
    output.findAll("span",attrs={"data-translate":"checking_browser"})
    if(len(output) > 0):
        validCookies = False
        print("Bad cookies detected: IUAM Cloud flair page")
        return True
    validCookies = True
    return False


validCookies = False

cookies = {
        "ppu_idelay_fc125c61ea24cf047efcf025783a7b67":"1",
        "ppu_main_fc125c61ea24cf047efcf025783a7b67":"1",
        "__cfduid":"d966b70d953153d8d5d2c5ead1f43bf031571019913",
        "cf_clearance":"bf4573110e25f3d187af9360c092e58650a09664-1571019918-0-150"}

headers = OrderedDict(
    (
        ("Host", None),
        ("Connection", "keep-alive"),
        ("Upgrade-Insecure-Requests", "1"),
        ("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"),
        (
            "Accept",
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        ),
        ("Accept-Language", "en-US,en;q=0.9"),
        ("Accept-Encoding", "gzip, deflate"),
    )
)

def getPage(url) -> requests:
    global cookies
    global headers
    r = requests.get(url,timeout=15)
    while(badCookies(r)):
        cookies = getCookies()
        r = requests.get(url,timeout=15)
        print(r.cookies)
        file = open("badcookie.html","w+")
        file.write(r.text)
        file.close()
    return r
    


def initFromUser() -> tuple:
    url = input("Site name?(EX www.kiss-anime.ws)\n> ")
    name = input("name of show:\n> ")
    start = input("which episode should I start at?\n>  ")
    end = input("what episode should I end at?\n> ")
    return (url,name,start,end)

taskSpecs = None

def main() -> None:
    global taskSpecs
    #taskSpecs = initFromUser()
    print(taskSpecs)
    url = "www.kiss-anime.ws"#taskSpecs[0]
    name = "one piece"#taskSpecs[1]
    start = 1#taskSpecs[2]
    end = 10#taskSpecs[3]
    url = "http://www.kiss-anime.ws/Anime/3-gatsu-no-lion"#UrlBuilder().useTls(True).setUrl(url).animeName(name).makeAnimeHomepage().build()
    with getPage(url) as response:
        file = open("homePage.html","w+")
        file.write(response.text)
        file.close()

def test():
    driver = webdriver.Firefox(executable_path="A:\geckodriver.exe")
    driver.get("https://kiss-anime.ws")
    driver.implicitly_wait(10)
    print(driver.get_cookies)

    print('hello')
    

#test()
main()